1. Ensure you have IAM user key-pair and region set up via 

```sh
aws configure
```

2. Ensure you have installed ansible 2.3.0+ and boto, boto3 libs

3. Run i.e.:

```sh
ansible-playbook -i inventory ${your_playbook} [-e state=present|absent] 
```